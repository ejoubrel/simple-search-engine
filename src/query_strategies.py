from abc import ABC, abstractmethod
from typing import List

from nltk.tokenize import word_tokenize


class QueryStrategy(ABC):
    
    @abstractmethod
    def process_query(self, query: str):
        """Abstract method to enforce method definition in subclasses."""


class StandardQueryStrategy(QueryStrategy):
    
    def process_query(self, query: str) -> List[str]:
        return [token.lower() for token in word_tokenize(query)]
