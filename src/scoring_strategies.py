from abc import ABC, abstractmethod
from math import log
from typing import List

from src.enums import FieldEnum
from src.services import (get_doc_len,
                          get_nb_occurences_in_doc,
                          get_nb_occurences_total,
                          get_nb_of_docs_present,
                          get_avg_doc_len)

class ScoringStrategy(ABC):
    
    @abstractmethod
    def compute_score(doc_id: str,
                      tokens: str,
                      docs: List[dict],
                      title_index: dict,
                      content_index: dict,
                      metadata_filepath: str):
        """Abstract method to enforce method definition in subclasses."""


class ScoringWeightedBM25(ScoringStrategy):
    
    def __init__(self, k1: float = 1.2, b: float = 0.75):
        self.k1 = k1
        self.b = b
    
    def _compute_bm25(self,
                      doc_id: str,
                      tokens: List[str],
                      field: FieldEnum,
                      index: dict,
                      docs: List[dict],
                      avg_doc_len: int):
        k1 = self.k1
        b = self.b
        doc_len = get_doc_len(doc_id, field, docs)
        score = 0
        for token in tokens:
            if doc_id in index[token].keys():
                nb_in_doc = get_nb_occurences_in_doc(token, doc_id, index)
                nb_tot = get_nb_occurences_total(token, index)
                freq_tot = get_nb_of_docs_present(token, index)
                
                idf = log(nb_tot / freq_tot)
                num = nb_in_doc * (k1 + 1)
                denum = nb_in_doc + k1 * (1 - b + b * (doc_len/avg_doc_len))
                
                score += idf * num / denum
        
        return score
     
    def compute_score(self,
                      doc_id: str,
                      tokens: str,
                      docs: List[dict],
                      title_index: dict,
                      content_index: dict,
                      metadata_filepath: str) -> float:
        avg_title_len = get_avg_doc_len(FieldEnum.TITLE,
                                        docs,
                                        metadata_filepath)
        avg_content_len = get_avg_doc_len(FieldEnum.CONTENT,
                                          docs,
                                          metadata_filepath)
        title_score = self._compute_bm25(doc_id=doc_id,
                                         tokens=tokens,
                                         field=FieldEnum.TITLE,
                                         index = title_index,
                                         docs=docs,
                                         avg_doc_len=avg_title_len)
        
        content_score = self._compute_bm25(doc_id=doc_id,
                                           tokens=tokens,
                                           field=FieldEnum.CONTENT,
                                           index=content_index,
                                           docs=docs,
                                           avg_doc_len=avg_content_len)
        
        return 1.5 * title_score + content_score
