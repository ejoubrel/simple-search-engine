from enum import Enum


class FieldEnum(Enum):
    TITLE = "title"
    CONTENT = "content"
