
from typing import List
import json
import os

from nltk.tokenize import word_tokenize

from src.enums import FieldEnum
from src.utils import load_json


def get_index_from_field(field: FieldEnum,
                         indexes: dict):
    return indexes[field.value]


def get_nb_occurences_in_doc(token: str, doc_id: str, index: dict):
    return index[token][doc_id]['count']


def get_nb_occurences_total(token: str, index: dict):
    return sum([val['count'] for val in index[token].values()])


def get_nb_of_docs_present(token: str, index: dict):
    return len(index[token])


def _create_metadata(docs: List[dict], metadata_filepath: str) -> None:
    data = {'avg_doc_length': {}}

    for field in FieldEnum:
        len_list = [len(word_tokenize(doc[field.value])) for doc in docs]
        data['avg_doc_length'][field.value] = sum(len_list) / len(len_list)
    
    with open(metadata_filepath, 'w', encoding='utf-8') as file:
        json.dump(data, file)


def get_avg_doc_len(field: FieldEnum,
                    docs: List[dict],
                    metadata_filepath: str) -> int:
    if os.path.isfile(metadata_filepath):
        data = load_json(metadata_filepath)
        if 'avg_doc_length' in data and field.value in data['avg_doc_length']:
            return data['avg_doc_length'][field.value]
    else:
        _create_metadata(docs, metadata_filepath)
        return get_avg_doc_len(field, docs, metadata_filepath)


def get_doc_len(doc_id: str, field: str, docs: List[dict]) -> int:
    for doc in docs:
        if str(doc['id']) == doc_id:
            return len(word_tokenize(doc[field.value]))

