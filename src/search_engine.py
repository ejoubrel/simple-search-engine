import json

from src.query_strategies import QueryStrategy, StandardQueryStrategy
from src.scoring_strategies import ScoringStrategy, ScoringWeightedBM25
from src.filter_strategies import FilterStrategy, AndFilterStrategy

from src.utils import load_json

class SearchEngine:
    
    def __init__(
        self,
        query: str,
        query_strategy: QueryStrategy = StandardQueryStrategy(),
        scoring_strategy: ScoringStrategy = ScoringWeightedBM25(),
        filter_strategy: FilterStrategy = AndFilterStrategy(),
        docs_filename: str = "input/documents.json",
        title_index_filename: str = "input/title_pos_index.json",
        content_index_filename: str = "input/content_pos_index.json",
        metadata_filename: str = "data/metadata.json"
    ):
        self.query_strategy = query_strategy
        self.scoring_strategy = scoring_strategy
        self.filter_strategy = filter_strategy
        
        self.tokens = self.query_strategy.process_query(query)
        
        self.docs = load_json(docs_filename)
        self.title_index = load_json(title_index_filename)
        self.content_index = load_json(content_index_filename)
        self.metadata_filename = metadata_filename
        
        self.ranked_results = None
    
    def ranking(self):
        common_ids = self.filter_strategy.filter_docs(self.tokens,
                                                      self.title_index,
                                                      self.content_index)
        
        temp = []
        for doc in self.docs:
            doc_id = str(doc['id'])
            if doc_id in common_ids:
                score = self.scoring_strategy.compute_score(
                    doc_id,
                    self.tokens,
                    self.docs,
                    self.title_index,
                    self.content_index,
                    self.metadata_filename
                )
                temp.append(
                    {
                        'title': doc['title'],
                        'url': doc['url'],
                        'score': score
                    }
                )
        
        sorted_docs = sorted(temp, key=lambda x: x['score'], reverse=True)
        
        result = [{key : val for key, val in doc.items() if key != 'score'} 
                  for doc in sorted_docs]
        self.ranked_results = {
            'results': result,
            'nb_documents': len(self.docs),
            'nb_filtered_docs': len(common_ids)
        }
    
    def save_ranking(self) -> None:
        with open('output/results.json', 'w', encoding='utf-8') as file:
            json.dump(self.ranked_results, file)
