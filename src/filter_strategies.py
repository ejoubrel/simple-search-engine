from abc import ABC, abstractmethod
from typing import List


class FilterStrategy(ABC):
    
    
    def _get_doc_ids(self,
                     token: str,
                     title_index: dict,
                     content_index: dict) -> List[str]:
        return list(content_index[token].keys()) +\
            list(title_index[token].keys())
    
    @abstractmethod
    def filter_docs(tokens: List[str],
                    title_index: dict,
                    content_index: dict) -> List[str]:
        """Abstract method to enforce method definition in subclasses."""
    

class AndFilterStrategy(FilterStrategy):
    
    def filter_docs(self,
                    tokens: List[str],
                    title_index: dict,
                    content_index: dict) -> List[str]:
        common_ids = set(
            self._get_doc_ids(tokens[0], title_index, content_index)
        ).intersection(
            *[self._get_doc_ids(token,
                                title_index,
                                content_index) for token in tokens]
        )
        return common_ids

class OrFilterStrategy(FilterStrategy):
    
    def filter_docs(self,
                    tokens: List[str],
                    title_index: dict,
                    content_index: dict) -> List[str]:
        common_ids = set(
            self._get_doc_ids(tokens[0], title_index, content_index)
        ).union(
            *[self._get_doc_ids(token,
                                title_index,
                                content_index) for token in tokens]
        )
        return common_ids