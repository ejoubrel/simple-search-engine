from src.search_engine import SearchEngine
from src.query_strategies import StandardQueryStrategy
from src.filter_strategies import OrFilterStrategy
import nltk
nltk.download('punkt')

def main(query: str):
    engine = SearchEngine(
        query=query,
    )
    engine.ranking()
    engine.save_ranking()


if __name__ == "__main__":
    query = "problème médical"
    main(query)