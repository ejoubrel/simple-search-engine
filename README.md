# Simple Search Engine


This project implements a simple search engine.
Given a list of crawled webpages split into fields, a positional inverted index for those fields and a user query, it provides a ranked list of corresponding webpages with their url and title.

## How it works

The user can enter a query and chose a filtering stategy for the engine:
- AndFilterStrategy : selected documents contain all words from query
- OrFilterStrategy : selected documents contain at least a word from query

NB: The filtering is applied on both title and content fields.

The query is tokenized, and documents in corpus filtered according to strategy.
For each filtered document, the engine computes a score for the document based on selected scoring strategy (At the moment, computing bm25 score for each field and giving more weight to title score)

The filtered documents are then sorted by descending score, and saved in a json file along with the total number of documents, and the number of filtered documents.


## Authors

- Elwenn Joubrel : [@ejoubrel](https://www.gitlab.com/ejoubrel)


## Installation

You will need python and pip, and after cloning this project:

```bash
  cd simple-search-engine
  pip install -r requirements.txt
  python app.main
```
